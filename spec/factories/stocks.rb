# frozen_string_literal: true

FactoryBot.define do
  factory :stock do
    name { FFaker::Name.name }
    bearer_name { FFaker::Name.name }
  end

  factory :invalid_stock, class: Stock do
    name { nil }
    bearer_name { FFaker::Name.name }
  end
end

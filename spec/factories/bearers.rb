# frozen_string_literal: true

FactoryBot.define do
  factory :bearer do
    name { FFaker::Name.name }
  end

  factory :invalid_bearer, class: Stock do
    name { nil }
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StocksController, type: :controller do
  let!(:stocks) { create_pair :stock }
  let!(:stock) { stocks.first }
  let!(:bearer_name) { stock.bearer.name }

  describe 'GET /index' do
    let(:perform_index) do
      get :index, params: { format: :json }
    end

    it 'returns success status code' do
      perform_index

      expect(response).to be_ok
    end

    it 'returns list of stocks' do
      perform_index

      expect(JSON.parse(response.body)['data'].size).to eq(2)
    end
  end

  describe 'POST /create' do
    context 'with valid attributes' do
      let(:perform_create) do
        post :create, params: { format: :json,
                                data: { type: 'stocks', attributes: attributes_for(:stock) } }
      end

      it 'returns status created' do
        perform_create

        expect(response).to have_http_status(:created)
      end

      it 'saves the new stock in the database' do
        expect { perform_create }.to change(Stock, :count).by(1)
      end

      it 'saves the new bearer in the database' do
        expect { perform_create }.to change(Bearer, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      let(:perform_create) do
        post :create, params: { format: :json, data: { type: 'stocks', attributes: attributes_for(:invalid_stock) } }
      end

      it 'returns status unprocessable_entity' do
        perform_create

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'does not save new stock in the database' do
        expect { perform_create }.not_to change(Stock, :count)
      end

      it 'does not save new bearer in the database' do
        expect { perform_create }.not_to change(Bearer, :count)
      end
    end

    context 'with empty bearer_name' do
      let(:perform_create) do
        post :create, params: { format: :json, data: { type: 'stocks', attributes: { name: 'Test', bearer_name: nil } } }
      end

      it 'returns status unprocessable_entity' do
        perform_create

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'does not save new stock in the database' do
        expect { perform_create }.not_to change(Stock, :count)
      end

      it 'does not save new bearer in the database' do
        expect { perform_create }.not_to change(Bearer, :count)
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid attributes' do
      let(:perform_update) do
        patch :update, params: { format: :json, id: stock.id,
                                 data: { type: 'stocks', attributes: { name: 'Test', bearer_name: 'Test' } } }
      end

      it 'returns status updated' do
        perform_update

        expect(response).to be_ok
      end

      it 'updates the stock in the database' do
        expect { perform_update }.to change { stock.reload.name }.to('Test')
      end

      it 'creates new bearer for stock in the database' do
        expect { perform_update }.to change(Bearer, :count).by(1)
        expect(stock.reload.bearer.name).to eq 'Test'
      end
    end

    context 'with stock name only' do
      let(:perform_update) do
        patch :update, params: { format: :json, id: stock.id,
                                 data: { type: 'stocks', attributes: { name: 'Test' } } }
      end

      it 'returns status updated' do
        perform_update

        expect(response).to be_ok
      end

      it 'updates the stock in the database' do
        expect { perform_update }.to change { stock.reload.name }.to('Test')
      end

      it 'creates new bearer for stock in the database' do
        expect { perform_update }.to change(Bearer, :count).by(0)
        expect(stock.reload.bearer.name).to eq bearer_name
      end
    end

    context 'with the existing bearer name' do
      let!(:another_bearer) { create :bearer }
      let(:perform_update) do
        patch :update,
              params: {
                format: :json, id: stock.id,
                data: { type: 'stocks', attributes: { name: 'Test', bearer_name: another_bearer.name } } }
      end

      it 'returns status updated' do
        perform_update

        expect(response).to be_ok
      end

      it 'updates the stock in the database' do
        expect { perform_update }.to change { stock.reload.name }.to('Test')
      end

      it 'updates bearer for stock in the database' do
        expect { perform_update }.to change(Bearer, :count).by(0)
        expect(stock.reload.bearer.name).to eq another_bearer.name
      end
    end

    context 'with invalid attributes' do
      let(:perform_update) do
        patch :update, params: { format: :json, id: stock.id, data: { type: 'stocks', attributes: { name: nil } } }
      end

      it 'returns status unprocessable_entity' do
        perform_update

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'does not update the stock' do
        expect { perform_update }.not_to change { stock.reload.name }
      end

      it 'does not create new bearer for stock in the database' do
        expect { perform_update }.not_to change(Bearer, :count)
        expect(stock.reload.bearer.name).to eq stock.bearer.name
      end
    end
  end

  describe 'DELETE /destroy' do
    context 'without deleted_at' do
      let(:perform_destroy) do
        delete :destroy, params: { format: :json, id: stock.id, data: { type: 'stocks' } }
      end

      it 'returns status no_content' do
        perform_destroy

        expect(response).to have_http_status(:no_content)
      end

      it 'updates the stock deleted_at' do
        expect { perform_destroy }.to change(Stock.active, :count).by(-1)
      end
    end

    context 'with deleted_at' do
      let(:deleted_stock) { create(:stock, deleted_at: Time.zone.yesterday) }

      let(:perform_destroy) do
        delete :destroy, params: { format: :json, id: deleted_stock.id, data: { type: 'stocks' } }
      end

      it 'returns status not_found' do
        perform_destroy

        expect(response).to have_http_status(:not_found)
      end

      it 'does not update the stock deleted_at' do
        expect { perform_destroy }.not_to change(Stock.active, :count)
      end
    end
  end
end

require 'rails_helper'

RSpec.describe Bearer, type: :model do
  subject(:bearer) { create :bearer }

  it { should have_many(:stocks).dependent(:destroy) }

  it { should validate_presence_of :name }
  it { should validate_uniqueness_of(:name) }
end

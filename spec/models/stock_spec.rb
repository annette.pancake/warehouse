require 'rails_helper'

RSpec.describe Stock, type: :model do
  let(:bearer) { create :bearer }
  subject(:stock) { create :stock, bearer: bearer }

  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name).scoped_to(:deleted_at) }
end

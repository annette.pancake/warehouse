class CreateStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :stocks do |t|
      t.string :name, null: false
      t.integer :bearer_id

      t.datetime :deleted_at

      t.timestamps
    end
  end
end

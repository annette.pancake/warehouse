class CreateBearers < ActiveRecord::Migration[5.2]
  def change
    create_table :bearers do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end

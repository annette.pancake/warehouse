# frozen_string_literal: true

class Stock < ApplicationRecord
  belongs_to :bearer

  validates :name, presence: true
  validates :name, uniqueness: { scope: :deleted_at }

  before_validation :assign_bearer, if: -> { bearer_name.present? }

  scope :active, -> { where(deleted_at: nil) }

  attr_accessor :bearer_name

  private

  def assign_bearer
    self.bearer = Bearer.find_or_create_by(name: bearer_name)
  end
end

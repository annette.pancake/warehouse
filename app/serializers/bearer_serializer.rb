# frozen_string_literal: true

class BearerSerializer < BaseSerializer
  attributes :id, :name
end

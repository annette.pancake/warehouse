# frozen_string_literal: true

class StockSerializer < BaseSerializer
  attribute :name

  attribute :bearer_name do |object|
    object.bearer.name
  end
end

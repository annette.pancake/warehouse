# frozen_string_literal: true

class BaseSerializer
  include FastJsonapi::ObjectSerializer

  def serialized_errors
    errors = @resource.errors.details.flat_map do |(attribute, error_types)|
      error_types.map do |error_type_hash|
        error_type    = error_type_hash[:error]
        short_message = @resource.errors.generate_message(attribute, error_type)
        full_message  = @resource.errors.full_message(attribute, short_message)

        {
          source: { pointer: "/data/attributes/#{attribute}" },
          code: "#{attribute}_#{error_type}",
          detail: full_message
        }
      end
    end

    wrap_errors(errors)
  end

  private

  def wrap_errors(errors)
    { errors: [] }.tap do |json|
      json[:errors] = Array.wrap(errors)
    end
  end
end

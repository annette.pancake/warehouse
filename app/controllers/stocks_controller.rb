# frozen_string_literal: true

class StocksController < ApplicationController
  before_action :find_stock, only: %i[update destroy]

  def index
    stocks = Stock.active

    render json: StockSerializer.new(stocks).serialized_json, status: :ok
  end

  def create
    stock = Stock.new(stock_params)

    if stock.save
      render json: StockSerializer.new(stock).serialized_json, status: :created
    else
      render json: StockSerializer.new(stock).serialized_errors, status: :unprocessable_entity
    end
  end

  def update
    if @stock.update(stock_params)
      render json: StockSerializer.new(@stock).serialized_json, status: :ok
    else
      render json: StockSerializer.new(@stock).serialized_errors, status: :unprocessable_entity
    end
  end

  def destroy
    @stock.update_column(:deleted_at, Time.zone.now)

    head :no_content
  end

  private

  def find_stock
    @stock = Stock.find_by!(id: params[:id], deleted_at: nil)
  rescue StandardError
    head :not_found
  end

  def stock_params
    params.require(:data).require(:attributes).permit(:name, :bearer_name)
  end
end
